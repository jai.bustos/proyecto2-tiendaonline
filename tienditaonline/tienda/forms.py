from django import forms
from django.forms import ModelForm
from .models import Producto
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Contacto    


class ProductoForm(ModelForm):

    class Meta:
        model = Producto
        fields =['nombre', 'marca','tipo_producto', 'imagen']


class CustomUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password1','password2']


class ContactoForm(ModelForm):

    class Meta:
        model = Contacto
        fields =['nombre', 'correo', 'motivo', 'mensaje', 'avisos']
