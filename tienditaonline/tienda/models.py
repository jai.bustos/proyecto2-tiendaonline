from django.db import models

# Create your models here.

class TipoProducto(models.Model):
    nombre = models.CharField(max_length=80)

    def __str__(selft):
        return selft.nombre



class Producto(models.Model):
    nombre=models.CharField(max_length=200)
    marca=models.CharField(max_length=100)
    tipo_producto=models.ForeignKey(TipoProducto, on_delete = models.CASCADE)
    imagen=models.ImageField(null=True, blank=True )
    
    def __str__(selft):
        return selft.nombre


motivo_contacto = [
    [0, "consulta"],
    [1, "sugerencia"],
    [2, "felicitaciones"],
    [3, "reclamo"]
]
class Contacto(models.Model):
    nombre = models.CharField(max_length=100)
    correo = models.EmailField()
    motivo = models.IntegerField(choices=motivo_contacto)
    mensaje = models.TextField()
    avisos = models.BooleanField()

    def __str__(selft):
        return selft.nombre
