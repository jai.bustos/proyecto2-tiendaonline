from django.shortcuts import render, redirect
from .models import Producto
from .forms import ProductoForm, CustomUserForm, ContactoForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, authenticate
from django.contrib import messages

from rest_framework import viewsets
from .serializers import ProductoSerializer
# Create your views here.

def home(request):
    return render(request,'tienda/inicio.html')

def listado_productos(request):
    productos=Producto.objects.all()
    data ={
        'productos':productos
    }
    return render(request,'tienda/listado_productos.html',data)

@permission_required('tienda.add_producto')
def nuevo_producto(request):
    data ={
        'form':ProductoForm()
    }

    if request.method == 'POST':
        formulario = ProductoForm(request.POST, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje'] = "Se ha guardado correctamente"
             
    return render(request,'tienda/nuevo_producto.html',data)

def modificar_producto(request,id):
    productos=Producto.objects.get(id=id)
    data ={
        'form':ProductoForm(instance=productos)
    }

    if request.method == 'POST':
        formulario = ProductoForm(data=request.POST, instance=productos, files=request.FILES)
        if formulario.is_valid():
            formulario.save()
            data['mensaje']="Modificación realizada"
            data['form'] = ProductoForm(instance=Producto.objects.get(id=id))
    return render(request,'tienda/modificar_producto.html',data)


def eliminar_producto(request,id):
    productos=Producto.objects.get(id=id)
    productos.delete()

    return redirect(to="listado_productos")

def registro_usuario(request):
    data ={
        'form':CustomUserForm()
    }
    if request.method=='POST':
        formulario=CustomUserForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            username=formulario.cleaned_data['username']
            password=formulario.cleaned_data['password1']
            user=authenticate(username=username, password=password)
            login(request, user)
            messages.success(request,"Te has registrado correctamente")
            return redirect(to="inicio")
        data["form"] = formulario    

    return render(request, 'registration/registro.html',data)

def contacto(request):
    data ={
        'form':ContactoForm()
    }
    if request.method =='POST':
        formulario = ContactoForm(data=request.POST)
        if formulario.is_valid():
            formulario.save()
            data["mensaje"] = "Mensaje Enviado"
        else:
            data["form"] = formulario        

    return render(request, 'tienda/contacto.html', data)


class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class=ProductoSerializer
