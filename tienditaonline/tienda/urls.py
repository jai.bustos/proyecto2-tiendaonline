from django.urls import path, include
from .views import home, listado_productos, nuevo_producto, modificar_producto, eliminar_producto, registro_usuario, contacto, ProductoViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('productos', ProductoViewSet)


urlpatterns =[
    path('', home, name="inicio"),
    path('listado-productos/',listado_productos, name="listado_productos"),
    path('nuevo-producto/',nuevo_producto, name="nuevo_producto"),
    path('modificar-producto/<id>/',modificar_producto, name="modificar_producto"),
    path('eliminar-producto/<id>/',eliminar_producto, name="eliminar_producto"),
    path('registro/',registro_usuario, name="registro_usuario"),
    path('contacto/', contacto, name="contacto"),
    path('api/', include(router.urls)),
]